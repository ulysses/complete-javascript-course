/*
// Lecture: passing functions as arguments

var years = [1990,1965,1937,2005,1998];

function arrayCalc(arr, fn) {
    var arrRes = [];

    for (var i = 0; i < arr.length; i++) {

        arrRes.push(fn(arr[i]));
    }
    return arrRes;
};

function calculateAge(el){
    return 2016 - el;
};

function isFullAge(el) {
    return el >= 18;
}


function maxHeartRate(el) {
    if (el >= 18 && el <=81){
        return Math.round(206.9 - (0.67 * el));
    }    else {
        return -1;
    }

}

var ages = arrayCalc(years, calculateAge);
console.log(ages);
var rates = arrayCalc(ages, maxHeartRate);
console.log(rates);

*/

/*

// Lecture: Functions returning functions

function interviewQuestion(job) {
    if(job === 'designer') {
        return function(name) {
            console.log(name + ', can you explain ux?')
        }
    } else if (job === 'teacher') {
        return function(name) {
            console.log(name + ', what do you teach?')
        }
    } else {
        return function(name) {
            console.log(name +', what you do?')
        }
    }
}


var teacherQuestion = interviewQuestion('teacher');
teacherQuestion('teacher');

interviewQuestion('designer')('carl');

*/

/*

// IFFE

function game() {
    var score = Math.random() * 10;
    console.log(score >= 5);
}

game();

(function () {
    var score = Math.random() * 10;
    console.log(score >= 5);
})();


(
    function (goodLuck) {
    var score = Math.random() * 10;
    console.log(score >= 5 - goodLuck);
    }
)(5);
*/

// Closures

/*

function retirement(retirementAge) {
    var a = ' years left till retirement';
    return function(yearOfBirth) {
        var age = 2016 - yearOfBirth;
        console.log(
            (retirementAge - age) +
            a);
    }
}

retirement(64)(1995);
*/

/*

// challenge

function interviewQuestion(job) {
    return function(name) {

        if(job === 'designer') {
            console.log(name + ', can you explain ux?')
            
        } else if (job === 'teacher') {
                console.log(name + ', what do you teach?')
            
        } else {
                console.log(name +', what you do?')
            
        }   
    }
}

interviewQuestion('teacher')('dennis');

*/

/*


// bind call and apply

var john = {
    name: 'John',
    age: 26,
    job: 'teacher',
    presentation: function(style,timeOfDay){
        if(style=== 'formal'){
            console.log('formal ' + timeOfDay + ' I ' + this.name);
        } else if(style=='friendly') {
            console.log('firendly' + ' I ' + this.name );
        } 
    }
};

john.presentation('formal', 'morning');


var emily = {
    name: 'Emily',
    age: 35,
    job: 'designer'
};

//call -- method borrowing 
john.presentation.call(emily, 'friendly', 'afternoon');

//bind
var johnFriendly = john.presentation.bind(john, 'friendly');
johnFriendly('afternoon');










var years = [1990,1965,1937,2005,1998];

function arrayCalc(arr, fn) {
    var arrRes = [];

    for (var i = 0; i < arr.length; i++) {

        arrRes.push(fn(arr[i]));
    }
    return arrRes;
};

function calculateAge(el){
    return 2016 - el;
};

function isFullAge(limit, el) {
    return el >= limit;
}

var ages = arrayCalc(years, calculateAge);
var fullJapan = arrayCalc(ages, isFullAge.bind(this, 20));

console.log(ages);
console.log(fullJapan);



*/


// coding challenge


function Question(question, answers, correct) {
    this.question = question;
    this.answers = answers;
    this.correct = correct;
    //this.displayQuestion = function(){
    //    var qNum = Math.round(Math.random() * 2);
    //    arrQuestions[qNum].ask();
    //}
}

Question.prototype.displayQuestion = function() {
    console.log(this.question);

    for (var i = 0; i < this.answers.length; i++) {
        console.log(this.answers[i])
    }
}

var q1 = new Question('Will Oleks kill Ben one day?', ['yes','no'], [0]);
var q2 = new Question('Is David Renz a serial killer?', ['yes', 'no'], [1]);
var q3 = new Question('Why is David Renz covered in cat hair?', ['he killed a cat and ate it','he likes to rub cat hair on himself'], [0]);

questions = [q1,q2,q3];

var qNum = Math.round(Math.random() * questions.length);

prompt(questions[qNum].displayQuestion());





//1. build a function constructer called Questions to describe a question

/*
var answers = {
    0: 'yes',
    1: 'no'
};
*/

function Question(question, answers, correct) {
    this.question = question;
    this.answers = answers;
    this.correct = correct;
    //this.displayQuestion = function(){
    //    var qNum = Math.round(Math.random() * 2);
    //    arrQuestions[qNum].ask();
    //}
}

Question.prototype.displayQuestion = function() {
    console.log(this.question);
    for (var i = 0; i < this.answers.length; i++) {
        console.log(i + ': ' + this.answers[i])
    }
}

Question.prototype.checkAnswer = function(ans) {
    if(ans === this.correct) {
        console.log('good job you are right')
    } else {
        console.log('try again!')
    }
}

var q1 = new Question('Will Oleks kill Ben one day?', ['yes','no'], [0]);
var q2 = new Question('Is David Renz a serial killer?', ['yes', 'no'], [1]);
var q3 = new Question('Why is David Renz covered in cat hair?', ['he killed a cat and ate it','he likes to rub cat hair on himself'], [0]);

questions = [q1,q2,q3];

var qNum = Math.round(Math.random() * questions.length);

questions[qNum].displayQuestion();

var answer = parseInt(prompt('Please select the correct answer'));

questions[qNum].checkAnswer();
