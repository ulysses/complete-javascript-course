// Function Constructor
/*
var john = {
    name: 'John',
    yearOfBirth: 1999,
    job: 'teacher'
};
*/

/*
var Person = function(name,yearOfBirth,job){
    this.name = name;
    this.yearOfBirth = yearOfBirth;
    this.job = job;
    this.calculateAge = function(){
        console.log(2016-this.yearOfBirth);
    }
}
*/

// if we want to insert via prototype
/*
var Person = function(name,yearOfBirth,job){
    this.name = name;
    this.yearOfBirth = yearOfBirth;
    this.job = job;
};

Person.prototype.calculateAge = function(){
    console.log(2016-this.yearOfBirth);
};

//property addition
Person.prototype.lastName = 'Smith';

var john = new Person('John', 1990, 'teacher');
var jane = new Person('Jane', 1969, 'designer');
var mark = new Person('Mark', 1948, 'retired');

john.calculateAge();
jane.calculateAge();
mark.calculateAge();


console.log(john.lastName);
console.log(jane.lastName);
console.log(mark.lastName);
*/

//Object.create
/*
var personProto = {
    calculateAge: function() {
        console.log(2016-this.yearOfBirth);
    }
};

var john = Object.create(personProto);
john.name = 'John';
john.yearOfBirth = 1990;
john.job = 'teacher';

var jane = Object.create(personProto,
    {
        name: {value: 'Jane'},
        yearOfBirth: {value: 1969},
        job: {value: 'designer'}
    });
*/


/*

// Primitives vs Objects

//primitives
var a = 23;
var b = a;
a = 46;
console.log(a);
console.log(b);


// objects
var obj1 = {
    name: 'John',
    age: 26
}

var obj2 = obj1;
obj1.age = 30;
console.log(obj1.age);
console.log(obj2.age);

// functions
//we dont pass objects into functions
//only primitives
var age = 27;
var obj = {
    name: 'Jonas',
    city: 'Lisbon'
};

function change(a, b){
    a = 30;
    b.city = 'San Francisco';
}

change(age,obj);

console.log(age);
console.log(obj.city);

*/


/*

///////// Lecture: Passing functions as arguments

var years = [1990,1965, 1937, 2005, 1998];

function arrayCalc(arr, fn) {
    var arrRes = [];
    for (var i = 0; i < arr.length;
        i++) {
            arrRes.push(fn(arr[i]));
        }
        return arrRes;
}

function calculateAge(el) {
    return 2016 - el;
}


function isFullAge (el) {
    return el >= 18;
}

function maxHeartRate(el) {
    if(el >= 18 && el <= 81) {
        return Math.round(206.9 - (0.67 * el));
    } else {
        return -1;
    }
    
}

// callback function, no parenthesis
var ages = arrayCalc(years,calculateAge);
console.log(ages);


var fullAges = arrayCalc(ages,isFullAge);
console.log(fullAges);

var rates = arrayCalc(ages, maxHeartRate);
console.log(rates);
*/

/*

///////// Lecture: Functions returning functions

function interviewQuestion(job){
    if(job === 'designer') {
        return function(name) {
            console.log(name + ', can you please explaine what UX design is?');
        }
     } else if (job === 'teacher') {
        return function(name) {
            console.log('what subject do you teach, ' + name + '?');
        }
     } else {
            return function (name) {
                console.log('Hello ' + name + ', what do you do?');
            }
        }
    }

var teacherQuestions = 
interviewQuestion('teacher');

var designerQuestions = 
interviewQuestion('designer');

teacherQuestions('John');
designerQuestions('John');
designerQuestions('Jane');
designerQuestions('Mark');
designerQuestions('Mike');

interviewQuestion('teacher')('Mark');

*/

////////////// Lecture: IIFE

/*
function game() {
    var score = Math.random() * 10;
    console.log(score >= 5);
}
game();
*/

/*

(function () {
    var score = Math.random() * 10;
    console.log(score >= 5);
})();

//console.log(score);

(function (goodLuck) {
    var score = Math.random() * 10;
    console.log(score >= 5 - goodLuck);
})(5);

*/

/////////////////// Lecture: closures

/*

function retirement(retirementAge) {
    var a = ' years left until retirement.';
    return function(yearOfBirth) {
        var age = 2016 - yearOfBirth;
        console.log((retirementAge - age) + a);
    } 
}

var retirementUS = retirement(66);
retirementUS(1990);
retirement(66)(1990);

var retirementGermany = retirement(65);
var retirementIceland = retirement(67);

retirementGermany(1990);
retirementUS(1990);
retirementIceland(1990);

// rewrite with closures

function interviewQuestion(job){
    if(job === 'designer') {
        return function(name) {
            console.log(name + ', can you please explaine what UX design is?');
        }
     } else if (job === 'teacher') {
        return function(name) {
            console.log('what subject do you teach, ' + name + '?');
        }
     } else {
            return function (name) {
                console.log('Hello ' + name + ', what do you do?');
            }
        }
    }


function interviewQuestion(job) {
    return function(name) {
        if(job === 'designer') {
            console.log(name + ', can you please explaine what UX design is?');
         } else if (job === 'teacher') {
            console.log('what subject do you teach, ' + name + '?');
         } else {
            console.log('Hello ' + name + ', what do you do?');
            }
    }
}

interviewQuestion('teacher')('John');

*/



// lecture: Bind, call and apply

/*
var john = {
    name: 'John',
    age : 26,
    job : 'teacher',
    presentation: function (style,timeOfDay) {
        if(style === 'formal') {
            console.log('Good ' + timeOfDay + 
            ' Ladies and Gentlemen! I\'m ' +
             this.name + ', I\'m a ' +
              this.job + ' and I\'m ' +
               this.age + ' years old.');
        } else if (style === 'friendly') {
            console.log('Hey! What\'s up? I\'m ' +
             this.name + ', I\'m a ' +
            this.job + ' and I\'m ' +
             this.age + ' years old. Have a nice ' + timeOfDay + '.');
        }
    }

};

var emily = {
    name: 'Emily',
    age: 35,
    job: 'designer'
};


john.presentation('formal', 'morning');
john.presentation.call(emily, 'friendly', 'afternoon');

//john.presentation.apply(emily, ['friendly', 'afternoon']);

var johnFriendly = john.presentation.bind(john, 'friendly');

johnFriendly('morning');
johnFriendly('night');

var emilyFormal = john.presentation.bind(emily, 'formal');
emilyFormal('afternoon');







var years = [1990,1965, 1937, 2005, 1998];

function arrayCalc(arr, fn) {
    var arrRes = [];
    for (var i = 0; i < arr.length;
        i++) {
            arrRes.push(fn(arr[i]));
        }
        return arrRes;
}

function calculateAge(el) {
    return 2016 - el;
}


function isFullAge (limit, el) {
    return el >= limit;
}

var ages = arrayCalc(years, calculateAge);

var fullJapan = arrayCalc(ages, isFullAge.bind(this,20));
console.log(ages);
console.log(fullJapan);

*/


////////////////CODING CHALLENGE


//1. Create function constructor

( function () {

    //score variable
    var score = 0;


    function Question(question, answer, correct) {
        this.question = question;
        this.answer = answer;
        this.correct = correct;
    };
    
    //2. create a couple of questions using the function constructor
    var q1 = new Question('Will Oleks kill Ben one day?', ['yes','no', 'maybe'], 2);
    var q2 = new Question('Is David Renz a serial killer?', ['yes', 'no', 'probably'], 2);
    var q3 = new Question('Why is David Renz covered in cat hair?',
     ['he killed a cat and ate it','he likes to rub cat hair on himself'], 1);
    
   
    // add prop
    Question.prototype.showQuestion = function() {
        console.log(this.question);
        for (i = 0; i < this.answer.length; i++) {
            console.log(i + ': ' + this.answer[i]);
        }
    }

    //6. check answer (add another prop)
    Question.prototype.checkAnswer = function(ans) {
        if(parseInt(ans) === this.correct) {
            console.log('good job you are a winner');            
        } else {
            console.log('go home loser');
        }
    }

    // track score
    Question.prototype.trackScore = function(ans){
        if(parseInt(ans) === this.correct) {
            score = score + 1;
            console.log('your current score is ' + score);            
        } else {
            console.log('your current score is ' + score);
        }
    }
    
    //3. store them in an array
    myQuestions = [q1,q2,q3];

    //8. function continue asking questions
    function nextQuestion() {
        console.log('---------------------------------')
        //4. select a random question with answers
        randNum = Math.round(Math.random() * myQuestions.length);
 
        //5. extract question as object in order to feed to prompt message
        var theQuestion = myQuestions[randNum].showQuestion();
        var myAnswer = prompt(myQuestions[randNum].question + ' Choose the correct answer');
        //console.log(myAnswer);
    
        myQuestions[randNum].checkAnswer(myAnswer);
        myQuestions[randNum].trackScore(myAnswer);

        //exit out of the game
        if(myAnswer== 'exit') {
            //break;
        } else {
        nextQuestion();
        }
        
    }
    
    nextQuestion();

})();





//9. include the option to quit the game if exit is entered


