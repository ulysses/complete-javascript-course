// lecture is about let and const
/**

// ES5 code
var name5 = 'Jane Smith';
var age5 = 23;
name5='Jane Miller';
console.log(name5);

// ES6
const name6 = 'Jane Smith';
let age6 = 23;
name6 = 'Jane Miller';
console.log(name6);

 

// ES5
function driversLicense5(passedTest) {
    if (passedTest) {
        var firstName = 'John';
        var yearOfBirth = '1990;'
        
    }

    console.log(firstName + ' born in ' + yearOfBirth + ' is no officially allowed to drive a car.');
}

driversLicense5(true);

// ES6
function driversLicense6(passedTest) {

    let firstName;
    const yearOfBirth = '1990';

    if (passedTest) {
        firstName = 'John';
    }
    console.log(firstName + ' born in ' + yearOfBirth + ' is no officially allowed to drive a car.');
}

driversLicense6(true);





let i = 23;

for (let i = 0; i < 5; i++) {
    console.log(i);
}

console.log(i);





// lecture: blocks and iifes

{
    const a = 1;
    let b = 2;
}

console.log(a + b);




// lecture: strings 

let firstName = 'john';
let lastName = 'Smith';
const yearOfBirth = 1990;

function calcAge(year) {
    return 2016 - year;
}

// es 5
console.log('annoying es5: this is ' + firstName + '. . . .')


//es6
console.log(`this is much better, i can write the ${firstName}, ${lastName}`)


const n = `${firstName} ${lastName}`;
console.log(n.startsWith('j'));
console.log(n.endsWith('h'));







// lecture arrow functions

const years = [1990,1965, 1982, 1937];

//es5
var ages5 = years.map(function(el){
    return 2016 - el;
});
console.log(ages5);

//es6
let ages6 = years.map(el => 2016 - el);
console.log(ages6);


ages6 = years.map((el, index) => `Age elemnt ${index + 1}: ${2016 - el}`);
console.log(ages6);

ages6 = years.map((el, index)=> {
    const now = new Date().getFullYear;
    const age = now - el;
    return `Age element ${index + 1}: ${age}`
})
console.log(ages6)






// arrow functions lexical 


// es 5 version
var box5 = {
    color: 'green',
    position: 1,
    clickMe: function() {

        var self = this;

        document.querySelector('.green').addEventListener('click', function(){
            var str = 'this is box number ' + self.position + ' and it is ' + self.color;
            alert(str); 
        })
    }
}

//box5.clickMe();



// es 6 version

const box6 = {
    color: 'green',
    position: 1,
    clickMe: function() {

        document.querySelector('.green').addEventListener('click', ()=> {
            var str = 'this is box number ' + this.position + ' and it is ' + this.color;
            alert(str); 
        })
    }
}

//box6.clickMe();



function Person(name) {
    this.name = name;
}

//es5
Person.prototype.myFriends5 = function(friends) {
    self = this
    var arr = friends.map(function(el){
       return self.name + ' is friends with ' + el;
    }
)
    console.log(arr);
}

var friends = ['Bob', 'Jane', 'Mark']

new Person('John').myFriends5(friends);




//es6

function Person(name) {
    this.name = name;
}
Person.prototype.myFriends6 = function(friends) {
    
   let arr = friends.map(el =>{
       return `${this.name} is friends with ${el}`;
    }
)
    console.log(arr);
}

let friends = ['Bob', 'Jane', 'Mark']

new Person('John').myFriends6(friends);
*/



//destructuring

// es5
/*
var john = ['John',26];
var name = john[0];
var age = john[1]


//es 6
const [name, age] = ['John', 26];
console.log(name);
console.log(age);



const obj = {
    firstName: 'John',
    lastName: 'Smith'
}

const {firstName, lastName} = obj;
console.log(firstName);
console.log(lastName);


const {firstName: a, lastName: b} = obj;
console.log(firstName);
console.log(lastName);



function calcAgeRetirment(year) {
    const age = new Date().getFullYear() - year;
    return [age, 65 - age];
}
const [age2, retirement] = 
calcAgeRetirment(1990);
console.log(age2);
console.log(retirement)






// lecture arrays

const boxes = document.querySelectorAll('.box');

//es5

var boxesArr5 = Array.prototype.slice.call(boxes);
boxesArr5.forEach(function(cur){
    cur.style.backgroundColor = 'dodgerblue'
});

//es6
const boxesArr6 = Array.from(boxes);
boxesArr6.forEach(cur=>cur.style.backgroundColor = 'dodgerblue');


//loop

//es5
/*
for(var i = 0;i < boxesArr5.length;i++) {
    if(boxesArr5[i].className === 'box blue') {
        continue;
    } else {
        boxesArr5[i].textContent = 'I changed to blue';
    }
}


//es6
for (const cur of boxesArr6) {
    if (cur.className === 'box blue') 
    {
        continue;
    }
    cur.textContent = 'I changed to blue!';
}


//es5
var ages = [12,17,8,21,14,11];
var full = ages.map(function(cur) {
    return cur >= 18
});
console.log(full);

console.log(full.lastIndexOf(true));
console.log(ages[full.lastIndexOf(true)]);

//es6
console.log(ages.findIndex(cur => cur >= 18));




// spread operator

function addFourAges(a,b,c,d) {
    return a + b +c +d;
}

var sum1 = addFourAges(18,30,12,21);
console.log(sum1);

//es5
var ages = [18,30,12,21];
var sum2 = addFourAges.apply(null, ages);
console.log(sum2);

//es 6
const sum3 = addFourAges(...ages);
console.log(sum3);

const familySmith = ['John', 'Jane', 'Mark'];
const familyMiller = ['Mary', 'Bob', 'Ann'];
const bigFamily = [...familySmith, ... familyMiller];
console.log(bigFamily);



const h = document.querySelector('h1');
const boxes = document.querySelectorAll('.box');

const all = [h, ...boxes];
Array.from(all).forEach(cur => cur.style.color = 'purple');
*/


// lecture rest parameters

//es5
/*
function isFullAge5() {
    //console.log(arguments);
    var argsArr = Array.prototype.slice.call(arguments);
    
    argsArr.forEach(function(cur){
        console.log((2016-cur) >=
        18);
    })
}

isFullAge5(1990, 1999, 1965);


//es6 
function isFullAge6(...years) {
    years.forEach(cur => console.log((2016-cur) >= 8));
}

isFullAge6(1990, 1999, 1965)



//ES5default parameters

function SmithPerson (firstName, yearOfBirth, lastName, nationality) {

lastName === undefined ? lastName = 'Smith' : lastName;

    this.firstName = firstName;
    this.lastName = lastName;
    this.yearOfBirth = yearOfBirth;
    this.nationality = nationality;
}

var john = new SmithPerson('John',1990);
console.log(john);


var emily = new SmithPerson('Emily', '1983', 'Diaz', 'sapnish')



//lecture: maps


const question = new Map();
question.set('question','What is the official name of the latest major JavaScript version?')


question.set(1, 'ES5');
question.set(2, 'ES6');
question.set(3, 'ES2015');
question.set('correct', 3);
question.set(true, 'Correct answer :D');
question.set(false, 'wrong')


console.log(question.get('question'))

console.log(question.size);

question.delete(2)




//classes

//es5

var Person5 = function(name, yearOfBirth, job) {
    this.name = name;
    this.yearOfBirth = yearOfBirth;
    this.job = job;
}

Person5.prototype.calculateAge = function() {
    var age = new Date().getFullYear - this.yearOfBirth;
    console.log(age);
}

var john5 = new Person5('John', 1990, 'teacher');


//ES6
class Person6 {
    constructor (name, yearOfBirth, job) {
        this.name = name;
        this.yearOfBirth = yearOfBirth;
        this.job = job;
    }

    calculateAge(){
        var age = new Date().getFullYear - this.yearOfBirth;
        console.log(age);
    }

    static greeting() {
        console.log('hey there!');
    }
}


const john6 = new Person6('John', 1990, 'teacher');
john6.calculateAge()

Person6.greeting();




//subcalsses

var Person5 = function(name, yearOfBirth, job) {
    this.name = name;
    this.yearOfBirth = yearOfBirth;
    this.job = job;
}

Person5.prototype.calculateAge = function() {
    var age = new Date().getFullYear() - this.yearOfBirth;
    console.log(age);
}

var Athlete5 = function(name, yearOfBirth, job, olympicGames, medals)
{
    Person5.call(this, name, yearOfBirth, job);
    this.olympicGames = olympicGames;
    this.medals = medals;
}

Athlete5.prototype = Object.create(Person5.prototype);

Athlete5.prototype.wonMedal = function() {
    this.medals++;
    console.log(this.medals);
}

var johnAthlete5 = new Athlete5('John', 1990, 'swimmer', 3, 10)

johnAthlete5.calculateAge();
johnAthlete5.wonMedal();

*/

//es6

class Person6 {
    constructor (name, yearOfBirth, job) {
        this.name = name;
        this.yearOfBirth = yearOfBirth;
        this.job = job;
    }

    calculateAge(){
        var age = new Date().getFullYear() - this.yearOfBirth;
        console.log(age);
    }
}

class Athlete6 extends Person6 {
    constructor(name, yearOfBirth, job, olympicGames, medals) {
        super(name, yearOfBirth, job);
        this.olympicGames = 
        olympicGames;
        this.medals = medals;
    }

    wonMedal() {
        this.medals++;
        console.log(this.medals);
    }
}


const  johnAthlete6 = new Athlete6('John', 1990, 'swimmer', 3, 10);

johnAthlete6.calculateAge();




































