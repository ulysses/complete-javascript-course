import Search from './models/Search';
import Recipe from './models/Recipe';
import List from './models/List';
import Likes from './models/Likes';
import * as searchView from './views/searchView';
import * as recipeView from './views/recipeView';
import * as listView from './views/listView';
import * as likesView from './views/likesView';
import { elements, renderLoader, clearLoader, elementStrings } from './views/base'



/**global state of the app 
- SSEarch object
- current recipe object
- shopping list object
- liked recipes

*/
const state = {};

/**SEARCH CONTROLLER */
const controlSearch = async () => {
    // 1) we should get a query from the view
    const query = searchView.getInput(); //do later
    //const query = 'pizza';
    //console.log(query)

    //create a new search object
    if(query) {
        // 2) new search object and add it to the state 
        state.search = new Search(query);

        // 3) prepare ui for results
        searchView.clearInput();
        searchView.clearResults();
        renderLoader(elements.searchRes);

        try {

        // 4) search for recipes
        await state.search.getResults();

        // 5) render the results on UI
        clearLoader();
        searchView.renderResults(state.search.result);

        } catch (err) {
            alert('Something went wrong . . .')
            clearLoader();
        }
    }
};


elements.searchForm.addEventListener('submit', e=>{
    e.preventDefault();
    controlSearch();
});


//TESTING
//window.addEventListener('load', e=>{
 //   e.preventDefault();
 //   controlSearch();
//});


elements.searchResPages.addEventListener('click', e => {
    const btn = e.target.closest('.btn-inline');
    if (btn) {
        const goToPage = parseInt(btn.dataset.goto,10);
        searchView.clearResults();
        searchView.renderResults(state.search.result, goToPage);
    }
});




/**RECIPE CONTROLLER */

const controlRecipe = async () => {
    //get id from url 
    const id = window.location.hash.replace('#', '');

    if(id) {    
        // prepare ui for changes
        recipeView.clearRecipe();
        renderLoader(elements.recipe);

        //highlight selected recipe
        if(state.search) searchView.highLightSelected(id);


        // create a new recipe object
        state.recipe = new Recipe(id);
        
        //TESTING 
        //window.r = state.recipe;

        try {
            // get the recipe data and parse ingredients
        await state.recipe.getRecipe();
        state.recipe.parseIngredients();

        // calculate servings and time
        state.recipe.calcTime();
        state.recipe.calcServings();

        // render recipe
        clearLoader();
        recipeView.renderRecipe(
            state.recipe,
            state.likes.isLiked(id)
            );

        } catch(err) {
            alert('error processing recipe!')
        }
        
    }

};



['hashchange','load'].forEach(event => window.addEventListener(event, controlRecipe));

/**
 * LIST CONTROLLER
 */
const controlList = () => {
    // create a new list if there is not yet
    if(!state.list) state.list = new List();

    // add each ingredient to the list
    state.recipe.ingredients.forEach(el=> {
        const item = state.list.addItem(el.count, el.unit, el.ingredient);
        listView.renderItem(item);
    });

}

// handle delete and update list item events
elements.shopping.addEventListener('click', e=> {
    const id = e.target.closest('.shopping__item').dataset.itemid;

    //handle the delete event
    if (e.target.matches('.shopping__delete, .shopping__delete *')) {
        // delete from state
        state.list.deleteItem(id);

        // delete from ui
        listView.deleteItem(id);

        // handle the count update
    } else if (e.target.matches('.shopping__count-value')) {
        const val = parseFloat(e.target.value, 10);
        state.list.updateCount(id, val);
    }
});



/**
 * LIKE CONTROLLER
 */


const controlLike = () => {
    if(!state.likes) state.likes = new Likes();
    const currentID = state.recipe.id;

    // user has not yet liked current recipe
    if (!state.likes.isLiked(currentID)) {
        // add the like to the data
        const newLike = state.likes.addLike(
            currentID,
            state.recipe.title,
            state.recipe.author,
            state.recipe.img
        );

        //toggle the like button 
        likesView.toggleLikeBtn(true);

        // add like to UI list
        likesView.renderLike(newLike);
        
    // user has liked current recipe
    } else {
        // remove the like to the data
        state.likes.deleteLike(currentID);
        //toggle the like button 
        likesView.toggleLikeBtn(false);

        // remove like to UI list
        likesView.deleteLike(currentID);

    }
    likesView.toggleLikeMenu(state.likes.getNumLikes());
};

//restore liked recipes on page load
window.addEventListener('load', ()=>{
    state.likes = new Likes();
    
    // restore likes
    state.likes.readStorage();

    //toggle the like button 
    likesView.toggleLikeMenu(state.likes.getNumLikes());

    //render existing likes
    state.likes.likes.forEach(like => likesView.renderLike(like));
});

// handling recipe button clicks
elements.recipe.addEventListener('click', e => {
    if(e.target.matches('.btn-decrease, .btn-decrease *')) {
        // decrease button is clicked
        if(state.recipe.servings > 1) {
            state.recipe.updateServings('dec');
            recipeView.updateServingsIngredients(state.recipe);
        }
        
    } else if(e.target.matches('.btn-increase, .btn-increase *')) {
        // decrease button is clicked
        state.recipe.updateServings('inc');
        recipeView.updateServingsIngredients(state.recipe);
    } else if (e.target.matches('.recipe__btn--add, .recipe__btn--add *')) {
        // add ingereidnet to shopping list
        controlList();
    } else if(e.target.matches('.recipe__love, .recipe__love *')) {
        //like controller
        controlLike();
    }
   
});
