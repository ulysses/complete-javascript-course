import axios from 'axios';
import { key, proxy } from '../config';

export default class Search {
    constructor(query) {
        this.query = query;
    }

    async getResults() {
        
    
        try{
        const res = await axios(`${proxy}https://www.food2fork.com/api/search?key=${key}&q=${this.query}`);
        this.result = res.data.recipes;
        } catch(error) {
            alert(error);
        }
    }
    
}

//https://www.food2fork.com/api/search
//85a7a5cc9f2ba5ffa8abab7e96c1db69 

