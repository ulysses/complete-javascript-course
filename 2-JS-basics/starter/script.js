/* 
*Variables and Data types
*/
/*
var firstName = 'John';
console.log(firstName);

var lastName = 'Smith';
var age = 28;

var fullAge = true;
console.log(fullAge);

var job;
console.log(job);

job = 'Teacher';
console.log(job);

// Variable naming rules 
var _3years = 3;
var johnMark = 'John and Mark';
var if = 23;
*/

/* 
*Variable Mutation and type coercion
*/


/*
var firstName = 'John';
var age = 28;

// Type coercion
console.log(firstName + ' ' + age);

var job, isMarried;
job = 'teacher';
isMarried = false;

console.log(firstName + ' is a ' + age + ' year old ' +
 job + '. Is here married? ' + isMarried)

// Variable mutation

age = 'twenty eight';
job = 'driver';

alert(firstName + ' is a ' + age + ' year old ' +
 job + '. Is here married? ' + isMarried)

var lastName = prompt('What is his last Name?'); 
console.log(firstName + ' ' + lastName);


*/

/*
Basic Operators
*/

/**
var year, yearJohn, yearMark
now = 2018; 
ageJohn = 28;
ageMark = 33;

// Math Operators
yearJohn = now - ageJohn;
yearMark = now - ageMark;

console.log(yearJohn);

console.log(now + 2);
console.log(now * 2);
console.log(now / 10);

// Logical Operators
var johnOlder = ageJohn > ageMark;
console.log(johnOlder);

// typeof operator
console.log(typeof johnOlder);
console.log(typeof ageJohn);
console.log(typeof 'Mark is older than John')
var x;
console.log(typeof x);  

 */

/**
 * Operator Precendence
 

var now = 2018;
var yearJohn = 1989;
var fullAge = 18;

// Multiple Operators
var isFullAge = now - yearJohn >= fullAge; //true
console.log(isFullAge)

// Grouping
var ageJohn = now - yearJohn;
var ageMark =  35;
var average = (ageJohn + ageMark) / 2;
console.log(average);

// Multiple Assignmnents
var x, y;
x = (3+5) * 4 - 6; // 8 * 4 - 6 // 32 - 6 // 26
//or
x = y = (3+5) * 4 - 6; // 8 * 4 - 6 // 32 - 6 // 26
console.log(x, y);

// More operators
x = x * 2;
// or
x *= 2; // same as above
console.log(x);
x += 10;
console.log(x);

//coding challenge

// BMI = mass / height^2 = mass/ (height * height)

// part 1

var markHeight, markMass, johnHeight, johnMass;
markHeight = 1.7;
markMass = 72.7;

johnHeight = 1.5;
johnMass = 69.4;

// part 2
var markBMI, jonnBMI;

markBMI = markMass/(markHeight^2);
johnBMI = johnMass/(johnHeight^2);

console.log("Mark's BMI is " + markBMI +
 " while " + " John's BMI is " + johnBMI)

// part 3
var markHigher;

markHigher = markBMI > johnBMI;

// part 4
console.log("Is Mark's BMI higher than John's? " + markHigher);

*/

/**
 *If / else statements
 * 
 

 var firstName = 'John';
 var civilStatus = 'single';

 if (civilStatus === 'married') {
     console.log(firstName + ' is married!');
 } else {
     console.log(firstName + ' will hopefully marry soon :)');
 }

 var isMarried = true;
if (isMarried) {
     console.log(firstName + ' is married!');
 } else {
     console.log(firstName + ' will hopefully marry soon :)');
 }

*/

/**
 * coding challenge rewrite as an if statement
 

if (markBMI > johnBMI) {
    console.log('Mark\'s BMI is higher than John\'s.');
} else {
    console.log('John\'s BMI is higher than Mark\'s.')
}

*/

/**
 * Boolean Logic
 

 var firstName = 'John';
 var age = 20;

 if (age < 13) {
     console.log(firstName + ' is a boy.');
 } else if (age >=13 && age <20) { //between 13 and 20
    console.log(firstName + ' is a teenager.');
 } else if (age >= 20 && age < 30) {
    console.log(firstName + ' a young man.');
 } else {
    console.log(firstName + ' is a man.');
 }

*/

/**
 * ternary operator and switch statements
 


 var firstName = 'John';
 var age = 14;

 // basically an if else statement 
 age >= 18 ?  console.log(firstName + ' drinks beer.')
 : console.log(firstName + ' drinks juice.');

 // assigning to a variable
 var drink = age >=18 ? 'beer' : 'juice';
console.log(drink);

// what if we had to use an if else statement instead
if (age >= 18) {
    var drink = 'beer';
} else {
    var drink = 'juice';
}

// switch statement (easier than ifelse statement)

var job = 'driver';
switch(job) {
    case 'teacher':
    case 'instructor': // basically we can have and or here
        console.log(firstName + ' teaches kids how to code.');
        break;
    case 'driver':
        console.log(firstName + ' drives an uber in Lisbon');
        break;
    case 'designer':
        console.log(firstName + ' designs beautiful websites');
        break;
    default:
        console.log(firstName + ' does something else.');
}


// change the below into a switch statement

var firstName = 'John';
 var age = 20;

 if (age < 13) {
     console.log(firstName + ' is a boy.');
 } else if (age >=13 && age <20) { //between 13 and 20
    console.log(firstName + ' is a teenager.');
 } else if (age >= 20 && age < 30) {
    console.log(firstName + ' a young man.');
 } else {
    console.log(firstName + ' is a man.');
 }

// here is the switch version
 switch(true) {
     case age < 13:
            console.log(firstName + ' is a boy.');
            break;
    case age >=13 && age <20:
            console.log(firstName + ' is a teenager.');
            break;
    case age >= 20 && age < 30:
            console.log(firstName + ' a young man.');
            break;
    default:
            console.log(firstName + ' is a man.');
 }


 */

/**
 * Truthy and Falsy values and equality operators
 

 // falsy values: undefined, null, 0, '', NaN
 // truthy values: NOT falsy values

 var height;

 height = 23;
 if (height || height ===0) {
     console.log('Variable is defined');
 } else {
     console.log('Variable has not been defined');
 }

// equality operators
// == vs === does type coercion
if(height == '23'){
    console.log('the == operator does type coercion');
}

*/


/**
 * coding challenge
 

// coding challenge number 2

// part 1 average score per team

var teamJohn, teamMike;
teamJohn = (89 + 120 + 103)/3;
teamMike = (116 + 94 + 123)/3;

// part 2 which team wins in average
teamJohn > teamMike ? console.log('the winner is teamJohn with avg of ' + teamJohn)
: console.log('the winner is teamMike with the avg of ' + teamMike);

//above was somewhat incomplete need to add a default aka else statement 

// part 3 change the scores to show different winners
// changing scores not needed

// part 4 add mary into the equation
var teamMary = (97 + 134 + 105)/3;
console.log(teamJohn, teamMike, teamMary);

if(teamJohn > teamMike && teamJohn > teamMike){
    console.log(teamJohn + ' wins')
} else if (teamMike > teamJohn && teamMike > teamMary) {
    console.log(teamMike + ' wins')
} else if (teamMary > teamJohn && teamMary > teamMike) {
    console.log(teamMary + ' wins')
} else {
    console.log('draw')
}

*/


/**
 * Functions
 

 function calculateAge(birthYear) {
     return 2018 - birthYear;
 }

 var ageJohn = calculateAge(1990);
 var ageMike = calculateAge(1948);
 var ageJane = calculateAge(1969);
 console.log(ageJohn, ageMike, ageJane);

function yearsUntilRetirement(year, firstName) {
    var age = calculateAge(year);
    var retirement = 65 - age;

    if(retirement > 0 ) {
        console.log(firstName + ' retires in ' + retirement + ' years.');
    } else {
        console.log(firstName + ' is already retired.')
    }    
}

yearsUntilRetirement(1990, 'John');
yearsUntilRetirement(1937, 'Herbert');

*/

/**
 * Function Statements and Expressions
 

// Function declaration
//function whatDoYouDo(job, firstName)

 // Function expression
var whatDoYouDo = function (job, firstName) {
    switch(job){
        case 'teacher':
            return firstName + ' teaches kids how to code'; //no break needed because return cancels it out
        case 'driver':
            return firstName + ' drives a cab in lisbon';
        case 'designer':
            return firstName + ' designs cool websites';
        default:
            return firstName + ' does something else';
    }
}

console.log(whatDoYouDo('teacher', 'John'))
console.log(whatDoYouDo('designer', 'Jane'))
console.log(whatDoYouDo('retired', 'Mark'))

*/


/**
 * arrays
 

 // initialize new array
 var names = ['John', 'Mark', 'Jane'];
 var years = new Array(1990, 1969, 1948); // not often used this way

 console.log(names[0]); //element 0 of the array
 console.log(names.length); // how many elements in the array

 // Mutate array data
 names[1] = 'Ben'; // changing the name in the array by index
 names[5] = 'Mary';
 console.log(names);

// different data types
var john = ['John', 'Smith', 1990, 'teacher', 'false'];

john.push('blue'); // push method will add element at the end of the array
john.unshift('Mr.'); // add in the beginning
john.pop(); //removes element from the end
john.pop(); //removes another from the end
john.shift(); // removes the first element
console.log(john);

console.log(john.indexOf(1990)); // passes the location of the item
console.log(john.indexOf(23)); // notice if it doesnt exist shows -1

var isDesigner = john.indexOf('designer') === -1 ? 'John is not a designer' :
 'john is a designer';

console.log(isDesigner);

*/


/**
 * CODING CHALLENGE 2 - TIP CALCULATOR
 

 // calculate tip

tipCalculator = function(bill){

    var percentage;
    
    if(bill < 50) {
        percentage =.2;  
    } else if (bill >50 && bill < 200) {
        percentage = .15;
    } else {
        percentage = .1;
    }
    return percentage * bill;
}

console.log(tipCalculator(10));


// run tip calculator through array
var bills = [124,48,268];

var tips = [tipCalculator(bills[0]),
            tipCalculator(bills[1]),
            tipCalculator(bills[2])
            ];

var total = [tips[0] + bills[0],
             tips[1] + bills[1],
             tips[2] + bills[2]
            ];

console.log(tips, total);            

*/


/**
 * objects and properties
 

 var john = {
     firstName: 'John',
     lastName: 'Smith',
     birthYear: 1990,
     family: ['Jane', 'Mark', 'Bob', 'Emily'],
     job: 'teacher',
     isMarried: false
 };
console.log(john.firstName); //dot is used to reach first part OR
console.log(john['lastName']);
// extracting with a variable
var x = 'birthYear';
console.log(john[x]);

//mutate the data on the fly
john.job = 'designer';
john['isMarried'] = true;
console.log(john);

// new object syntax
var jane = new Object(); // create a new empty object
jane.name = 'Jane';
jane.birthYear = 1969;
jane['lastName'] = 'Smith';
console.log(jane);

*/

/**
 * objects and methods
 

var john = {
    firstName: 'John',
    lastName: 'Smith',
    birthYear: 1992,
    family: ['Jane', 'Mark', 'Bob', 'Emily'],
    job: 'teacher',
    isMarried: false,
    calcAge: function(birthYear) { //we basically add the calculation in a function
        return 2018 - this.birthYear
    }
}; 

//here we input a value
console.log(john.calcAge(1990));

//if we change with the addition of 'this.' above . . . 
console.log(john.calcAge());

//what if we want to store it in the object?
john.age = john.calcAge() //here we add this into the object

//OR

var john = {
    firstName: 'John',
    lastName: 'Smith',
    birthYear: 1992,
    family: ['Jane', 'Mark', 'Bob', 'Emily'],
    job: 'teacher',
    isMarried: false,
    calcAge: function(birthYear) { //we basically add the calculation in a function
        this.age =  2018 - this.birthYear
    }
};

john.calcAge();
console.log(john);

*/

/**
 * coding challenge 4
 

//BMI  = mass / height ^2

var John = {
    fullName: 'John Smith',
    mass: 78,
    height: 1.7,
    //BMI: 100,
    calcBMI: function() { 
        this.bmi = this.mass/(this.height ^2)
        //return this.bmi;
    }
};

var Mark = {
    fullName: 'Mark Taylor',
    mass: 73,
    height: 1.4,
    //BMI: 100,
    calcBMI: function() { 
        this.bmi = this.mass/(this.height ^2)
        //return this.bmi;
    }
};

John.calcBMI();
Mark.calcBMI();
console.log(John, Mark);

console.log(John.bmi, Mark.bmi);

// who has the highest BMI?
var highestBMI = function(){
    if(John.bmi > Mark.bmi){
        console.log('John has higher bmi of ' + John.bmi);
    } else if (Mark.bmi >John.bmi) {
        console.log('Mark has higher bmi of ' + Mark.bmi);
    } else {
        console.log('They both have the same BMI');
    }

}

highestBMI();

*/

/**
 * loops and iterations
 

 for (var i = 0; i < 10; i++) {
     console.log(i);
 }

 // i  = 0, 0 < 10 true, log i to console, i++
 // i = 1, 1 < 10 true, log i to the console, i++
 // . . . 
 // i = 9, 9 < 10 true, log i to the console, i++
// i = 10, 10 < 10 FALSE, exit the loop

// count to 20 instead
for (var i = 0; i < 20; i++) {
    console.log(i);
}

// start from 1 instead
for (var i = 0; i < 20; i++) {
    console.log(i);
}

// change operator make less than or equal and increase by 2
for (var i = 0; i <= 20; i+=2) {
    console.log(i);
}

// loop through an array and extract the elements
var john = ['John', 'Smith', 1990, 'designer', 'false'];
for (var i = 0; i < john.length; i++) {
    console.log(john[i]);
}

//while loop 
var i = 0;
while (i < john.length) {
    console.log(john[i]);
    i++;
}


// continue and break statements
//below we skip the 1990 and false since they are not string
var john = ['John', 'Smith', 1990, 'designer', 'false'];
for (var i = 0; i < john.length; i++) {
    if (typeof(john[i]) !== 'string') continue; //if not a string continue
    console.log(john[i]);
}

// break 
// exits the loop and does not continue
var john = ['John', 'Smith', 1990, 'designer', 'false'];
for (var i = 0; i < john.length; i++) {
    if (typeof(john[i]) !== 'string') break; //if not a string continue
    console.log(john[i]);
}

// challenge (loop through array but in reverse)
var john = ['John', 'Smith', 1990, 'designer', 'false'];
for (var i = john.length - 1; i >= 0; i--) {
    //if (typeof(john[i]) !== 'string') continue; //if not a string continue
    console.log(john[i]);
}

*/

/**

// coding challenge 5

// tip function for John
tipCalculatorJohn = function(bill){
    var percentage;
    if(bill < 50) {
        percentage =.2;  
    } else if (bill >50 && bill < 200) {
        percentage = .15;
    } else {
        percentage = .1;
    }
    return percentage * bill;
}

//part 1
var billObject = {
    bills: [124,48,268,180,42],
    tips: [],
    tot: [],
    calcTipTot: function() {
        for(i = 0; i < this.bills.length; i++) {
            this.tips.push(tipCalculatorJohn(this.bills[i]));
            this.tot.push(tipCalculatorJohn(this.bills[i]) + this.bills[i]);

        }    
    }
};

//billObject.tips = billObject.calcTip;
billObject.calcTipTot();
console.log(billObject);



// his way of solving it

var john = {
    fullName: 'John Smith',
    bills: [124,48,268,180,42],
    calcTips: function() {
        this.tips = [];
        this.finalValues = [];

        for(i = 0; i < this.bills.length; i++){
        var percentage;
        var bill = this.bills[i]
        
        if(bill < 50) {
            percentage =.2;  
        } else if (bill >50 && bill < 200) {
            percentage = .15;
        } else {
            percentage = .1;
        }
        this.tips[i] = bill * percentage;
        this.finalValues = bill + bill * percentage;
        }
    }
}

john.calcTips();
console.log(john);

 */


/**
 * 
 */













 









































































