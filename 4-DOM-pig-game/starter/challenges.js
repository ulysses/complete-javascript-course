/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game

*/

var scores, roundScore, activePlayer, gamePlaying, winScore, prevDice,finalRoll;
init();

//calling an anonymous function
document.querySelector('.btn-roll').addEventListener('click',function(){

    

    // game is playing so should be true
   if(gamePlaying){


    console.log('NEWLINE . . ');
    console.log('prev fr ' + finalRoll); // will show 12 when 6 is rolled before
    //1. we need a random number
    var dice = Math.floor(Math.random() * 6) + 1;
    var dice2 = Math.floor(Math.random() * 6) + 1;
    var diceTotal = dice + dice2; 
    console.log('cur Dice ' + dice);
    //store previous roll
    dice === 6 ? prevDice = dice : prevDice = 0;
    //console.log(prevDice);
    //create the finalRoll value  
    if(dice=== prevDice){
        finalRoll = prevDice + finalRoll;
    } else {
        finalRoll = 0;
    }
    console.log('new fr ' + finalRoll);

    //2. display the result
    var diceDOM = document.querySelector('.dice')
    diceDOM.style.display = 'block';
    diceDOM.src = 'dice-' + dice + '.png';

    //2. display the result for the second dice
    var diceDOM2 = document.querySelector('.dice2')
    diceDOM2.style.display = 'block';
    diceDOM2.src = 'dice-' + dice2 + '.png';

    //3. update the round score but only if the rolled number is not a 1
        // OR if the 2 consecutive rolls aren't both 6 aka finalRoll is not 12
    if(dice !== 1 && dice2 !==1 && finalRoll !==12) {//   
        //add score
        roundScore += diceTotal; // this was defined in global scope
        document.querySelector('#current-' + activePlayer).textContent = roundScore;
    } else {
        //next player
        nextPlayer();
    }
}

});


document.querySelector('.btn-hold').addEventListener('click',function(){

    if(gamePlaying){
    // 1. add the current score to the global score
    scores[activePlayer] += roundScore;

    // 2. update the UI 
    document.querySelector('#score-' + activePlayer).textContent = scores[activePlayer];

    // 3. check if the player won the game 
    if(scores[activePlayer] >= winScore) {
        document.querySelector('#name-' + activePlayer).textContent = 'Winner!';
        document.querySelector('.dice').style.display = 'none';
        document.querySelector('.player-' + activePlayer + '-panel').classList.add('winner');
        document.querySelector('.player-' + activePlayer + '-panel').classList.remove('active');
        gamePlaying = false;
    } else {
    // 4. next player
    nextPlayer();
    }

    }

});



function nextPlayer(){
    //next player
    activePlayer===0 ? activePlayer = 1 : activePlayer = 0; //ternary operator
    roundScore = 0;
    document.getElementById('current-0').textContent = '0';
    document.getElementById('current-1').textContent = '0';

    document.querySelector('.player-0-panel').classList.toggle('active'); 
    document.querySelector('.player-1-panel').classList.toggle('active');
    //document.querySelector('.player-0-panel').classList.remove('active'); // pass the class you want to remove aka the grey
    //document.querySelector('.player-1-panel').classList.add('active');

    document.querySelector('.dice').style.display = 'none';
    document.querySelector('.dice2').style.display = 'none';
}


document.querySelector('.btn-new').addEventListener('click', init);


function init(){
// everytime someone submits a new winning score we will restart the game with the new score
winScore = document.getElementById("myNumber").value;

scores = [0,0]; 
activePlayer = 0;
roundScore = 0;
gamePlaying = true;

// we don't want to see the dice when the game starts
document.querySelector('.dice').style.display = 'none';
document.querySelector('.dice2').style.display = 'none';

//use get element by ID element which is faster 
document.getElementById('score-0').textContent = '0';
document.getElementById('score-1').textContent = '0';
document.getElementById('current-0').textContent = '0';
document.getElementById('current-1').textContent = '0';

// when we start a new game we want to relabel players 1 and 2
document.getElementById('name-0').textContent = 'Player 1';
document.getElementById('name-1').textContent = 'Player 2';

//remove winner
document.querySelector('.player-0-panel').classList.remove('winner');
document.querySelector('.player-1-panel').classList.remove('winner');
document.querySelector('.player-0-panel').classList.remove('active');
document.querySelector('.player-1-panel').classList.remove('active');
document.querySelector('.player-0-panel').classList.add('active');
}

 


/*
EXTRAS

//dice = Math.floor(Math.random() * 6) + 1;
//console.log(dice);

//document.querySelector('#current-' + activePlayer).textContent = dice; 
//document.querySelector('#current-' + activePlayer).innerHTML = '<em>' + dice + '</em>';

//var x = document.querySelector('#score-0').textContent;
//console.log(x);

/*
function btn(){
    //Do something here

}

// calling a function through the evenListenre
document.querySelector('.btn-roll').addEventListener('click',btn);
*/


